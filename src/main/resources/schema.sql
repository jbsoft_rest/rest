DROP TABLE tasks IF EXISTS;

create table tasks (
   id varchar(37) PRIMARY KEY,
   status varchar(255) not null,
   update_time varchar(255) not null
);