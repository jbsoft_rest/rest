package ru.mts.test.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.mts.test.rest.IdUtils;
import ru.mts.test.rest.dao.TaskDAO;
import ru.mts.test.rest.entity.ResponseTaskId;
import ru.mts.test.rest.entity.ResponseTaskStatus;
import ru.mts.test.rest.entity.Task;
import ru.mts.test.rest.scheduler.SchedulerComponent;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.atomic.AtomicLong;

import static javax.servlet.http.HttpServletResponse.*;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
@RestController
public class TaskController {
    @Autowired
    private TaskDAO taskDAO;
    @Autowired
    private SchedulerComponent scheduler;

    @RequestMapping(name = "/task", method = RequestMethod.POST)
    public ResponseTaskId post(@Nonnull HttpServletResponse response) {
        Task task = taskDAO.create();
        response.setStatus(SC_ACCEPTED);
        scheduler.create(task.getId());
        return new ResponseTaskId(task);
    }

    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
    public ResponseTaskStatus get(@PathVariable("id") String id, HttpServletResponse response) {
        if (!IdUtils.isValid(id)) {
            response.setStatus(SC_BAD_REQUEST);
            return null;
        }

        Task task = taskDAO.findById(id);
        // find in DB
        if (task == null) {
            response.setStatus(SC_NOT_FOUND);
            return null;
        }

        return new ResponseTaskStatus(id, task.getStatus(), task.getTimestamp());
    }

}
