package ru.mts.test.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
@JsonIgnoreProperties({"status", "timestamp"})
public class ResponseTaskId extends Task {
    public ResponseTaskId(Task task) {
        super(task);
    }

    @Override
    public String toString() {
        return String.format(
                "ResponseTaskId[id=%s]",
                id);
    }
}
