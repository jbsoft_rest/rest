package ru.mts.test.rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.annotation.Nonnull;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
@JsonIgnoreProperties({"id"})
public class ResponseTaskStatus extends Task {

    public ResponseTaskStatus(@Nonnull String id, @Nonnull String status, @Nonnull String timestamp) {
        super(id, status, timestamp);
    }

    @Override
    public String toString() {
        return String.format(
                "Task[status=%s, timestamp='%s']",
                status, timestamp);
    }
}
