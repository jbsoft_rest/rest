package ru.mts.test.rest.entity;

import javax.annotation.Nonnull;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
public class Task {
    @Nonnull
    final String id;
    @Nonnull
    final String status;
    @Nonnull
    final String timestamp;

    public Task(@Nonnull String id, @Nonnull String status, @Nonnull String timestamp) {
        this.id = id;
        this.status = status;
        this.timestamp = timestamp;
    }

    Task(@Nonnull Task task) {
        this.id = task.getId();
        this.status = task.getStatus();
        this.timestamp = task.getTimestamp();
    }

    @Nonnull
    public String getId() {
        return id;
    }

    @Nonnull
    public String getStatus() {
        return status;
    }

    @Nonnull
    public String getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return String.format(
                "Task[id=%s, status=%s, timestamp='%s']",
                id, status, timestamp);
    }
}
