package ru.mts.test.rest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
public final class IdUtils {
    @Nonnull
    public static String generateId() {
        return UUID.randomUUID().toString();
    }

    public static boolean isValid(@Nonnull String id) {
        try {
            UUID.fromString(id);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}