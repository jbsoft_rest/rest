package ru.mts.test.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.mts.test.rest.IdUtils;
import ru.mts.test.rest.entity.Task;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.Instant;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
@Repository
public class TaskDAOImpl implements TaskDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Nonnull
    @Override
    public Task create() {
        Task task = new Task(IdUtils.generateId(), "created", Instant.now().toString());
        jdbcTemplate.update(
                "INSERT INTO tasks (id, status, update_time) VALUES (?, ?, ?)",
                task.getId(), task.getStatus(), task.getTimestamp()
        );
        return task;
    }

    @Nullable
    @Override
    public Task findById(@Nonnull String taskId) {
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT status, update_time FROM tasks WHERE id = ?", new Object[]{taskId},
                    (rs, rowNum) -> new Task(taskId, rs.getString("status"), rs.getString("update_time"))
            );
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void updateStatus(@Nonnull String taskId, @Nonnull String status) {
        jdbcTemplate.update(
                "UPDATE tasks SET status = ?, update_time = ? WHERE id = ?",
                status, Instant.now().toString(), taskId
        );
    }
}
