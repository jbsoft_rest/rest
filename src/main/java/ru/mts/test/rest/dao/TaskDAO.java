package ru.mts.test.rest.dao;

import ru.mts.test.rest.entity.Task;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
public interface TaskDAO {
    @Nonnull
    Task create();

    @Nullable
    Task findById(@Nonnull String taskId);

    void updateStatus(@Nonnull String taskId, @Nonnull String status);
}
