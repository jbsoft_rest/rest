package ru.mts.test.rest.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mts.test.rest.dao.TaskDAO;

import javax.annotation.Nonnull;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Alexander Barsukov (abarsukov) on 11/10/2018.
 */
@Component
public class SchedulerComponent {
    @Autowired
    private TaskDAO taskDAO;

    private static final int DELAY_2_MINUTES = 2 * 60 * 1000;

    public void create(@Nonnull String taskId) {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                taskDAO.updateStatus(taskId, "finished");
            }
        };
        Timer timer = new Timer(taskId);

        timer.schedule(timerTask, DELAY_2_MINUTES);
        taskDAO.updateStatus(taskId, "running");
    }
}
